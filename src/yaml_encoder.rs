//
// yaml encoder helpers
//

// ----------------------------------------------------------------------------
// external interface
// ----------------------------------------------------------------------------
pub trait IntoYaml {
    fn intoyaml(self) -> Yaml;
}
#[derive(Default)]
pub struct MapEncoder {
    data: LinkedHashMap<Yaml, Yaml>,
    // simple counter to generate unique hash keys for comments and newlines
    // which may be intended duplicates
    uid: usize,
}
#[derive(Default)]
pub struct ListEncoder {
    inlined: bool,
    data: Vec<Yaml>,
}
// ----------------------------------------------------------------------------
// internals
// ----------------------------------------------------------------------------
use std::collections::HashMap;
use std::iter::FromIterator;

use super::linked_hash_map::LinkedHashMap;
use super::indexmap::IndexMap;

use super::yaml_rust::Yaml;
// ----------------------------------------------------------------------------
impl IntoYaml for Yaml {
    fn intoyaml(self) -> Yaml {
        self
    }
}
// ----------------------------------------------------------------------------
impl IntoYaml for MapEncoder {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self.data.len() {
            0 => Yaml::Null,
            _ => Yaml::Hash(self.data),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for ListEncoder {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self.data.len() {
            0 => Yaml::Null,
            _ => Yaml::Array(self.inlined, self.data),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl MapEncoder {
    // ------------------------------------------------------------------------
    pub fn new() -> MapEncoder {
        MapEncoder {
            data: LinkedHashMap::new(),
            uid: 0,
        }
    }
    // ------------------------------------------------------------------------
    pub fn from_key_value<N: Into<String>, T: IntoYaml>(name: N, data: T) -> MapEncoder {
        let mut encoder = MapEncoder::new();

        let data = data.intoyaml();
        if data != Yaml::Null {
            encoder.data.insert(Yaml::String(name.into()), data);
        }

        encoder
    }
    // ------------------------------------------------------------------------
    pub fn from_map<T: IntoYaml>(data: HashMap<String, T>) -> MapEncoder {
        let mut encoder = MapEncoder::new();

        for (key, value) in data {
            encoder.data.insert(Yaml::String(key), value.intoyaml());
        }

        encoder
    }
    // ------------------------------------------------------------------------
    pub fn add<N: Into<String>, T: IntoYaml>(&mut self, name: N, data: T) {
        let data = data.intoyaml();
        if data != Yaml::Null {
            self.data.insert(Yaml::String(name.into()), data);
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_comment<C: Into<String>>(&mut self, comment: C) {
        self.uid += 1;
        self.data.insert(
            Yaml::Comment(self.uid, format!(" {}", comment.into())),
            Yaml::Null,
        );
    }
    // ------------------------------------------------------------------------
    pub fn add_newline(&mut self) {
        self.uid += 1;
        self.data.insert(Yaml::NewLine(self.uid), Yaml::Null);
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl ListEncoder {
    // ------------------------------------------------------------------------
    pub fn new() -> ListEncoder {
        ListEncoder {
            inlined: false,
            data: Vec::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn new_inlined() -> ListEncoder {
        ListEncoder {
            inlined: true,
            data: Vec::new(),
        }
    }
    // ------------------------------------------------------------------------
    pub fn from_iter_inlined<T, I: IntoIterator<Item = T>>(input: I) -> ListEncoder
    where
        T: IntoYaml,
    {
        let mut encoder = ListEncoder::from_iter(input);
        encoder.inlined = true;
        encoder
    }
    // ------------------------------------------------------------------------
    pub fn add<T: IntoYaml>(&mut self, data: T) {
        let data = data.intoyaml();
        if data != Yaml::Null {
            self.data.push(data);
        }
    }
    // ------------------------------------------------------------------------
    pub fn add_comment<C: Into<String>>(&mut self, comment: C) {
        self.data
            .push(Yaml::Comment(0, format!(" {}", comment.into())));
    }
    // ------------------------------------------------------------------------
    pub fn add_newline(&mut self) {
        self.data.push(Yaml::NewLine(0));
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T> FromIterator<T> for ListEncoder
where
    T: IntoYaml,
{
    // ------------------------------------------------------------------------
    fn from_iter<I: IntoIterator<Item = T>>(iter: I) -> Self {
        let iter = iter.into_iter();
        let mut encoder = ListEncoder {
            inlined: false,
            data: Vec::with_capacity(iter.size_hint().0),
        };

        for element in iter {
            encoder.data.push(element.intoyaml())
        }

        encoder
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// Primitives
// ----------------------------------------------------------------------------
impl IntoYaml for bool {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Boolean(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> IntoYaml for &'a str {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.is_empty() {
            Yaml::Null
        } else {
            Yaml::String(self.to_owned())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<'a> IntoYaml for &'a String {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.is_empty() {
            Yaml::Null
        } else {
            Yaml::String(self.to_owned())
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for String {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.is_empty() {
            Yaml::Null
        } else {
            Yaml::String(self)
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for i8 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for i16 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for i32 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for i64 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(self)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for u8 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for u16 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for u32 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(i64::from(self))
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for usize {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        Yaml::Integer(self as i64)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for f32 {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        // bad bad hack
        let mut s = format!("{:.10}", self).trim_end_matches('0').to_string();
        if s.ends_with('.') {
            s.push('0');
        }
        Yaml::Real(s)
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for (f32, f32) {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.0 != 0.0 || self.1 != 0.0 {
            Yaml::Array(true, vec![self.0.intoyaml(), self.1.intoyaml()])
        } else {
            Yaml::Null
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for (f32, f32, f32) {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.0 != 0.0 || self.1 != 0.0 || self.2 != 0.0 {
            Yaml::Array(
                true,
                vec![self.0.intoyaml(), self.1.intoyaml(), self.2.intoyaml()],
            )
        } else {
            Yaml::Null
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl IntoYaml for (f32, f32, f32, f32) {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        if self.0 != 0.0 || self.1 != 0.0 || self.2 != 0.0 || self.3 != 0.0 {
            Yaml::Array(
                true,
                vec![
                    self.0.intoyaml(),
                    self.1.intoyaml(),
                    self.2.intoyaml(),
                    self.3.intoyaml(),
                ],
            )
        } else {
            Yaml::Null
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T> IntoYaml for Option<T>
where
    T: IntoYaml,
{
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self {
            Some(data) => data.intoyaml(),
            None => Yaml::Null,
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
impl<T> IntoYaml for Vec<T>
where
    T: IntoYaml,
{
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self.len() {
            0 => Yaml::Null,
            _ => ListEncoder::from_iter(self).intoyaml(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[allow(clippy::implicit_hasher)]
impl<T> IntoYaml for HashMap<String, T>
where
    T: IntoYaml,
{
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self.len() {
            0 => Yaml::Null,
            _ => MapEncoder::from_map(self).intoyaml(),
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
#[allow(clippy::implicit_hasher)]
impl<T> IntoYaml for IndexMap<String, T> where T: IntoYaml {
    // ------------------------------------------------------------------------
    fn intoyaml(self) -> Yaml {
        match self.len() {
            0 => Yaml::Null,
            _ => {
                let mut encoder = MapEncoder {
                    data: LinkedHashMap::new(),
                    uid: 0,
                };

                for (key, value) in self.into_iter() {
                    encoder.data.insert(Yaml::String(key), value.intoyaml());
                }
                encoder.intoyaml()
            }
        }
    }
    // ------------------------------------------------------------------------
}
// ----------------------------------------------------------------------------
